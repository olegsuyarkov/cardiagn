import sqlite3 as sql
import os
import configparser


config = configparser.ConfigParser()
config.read(os.getenv('CONFIG_PATH'))
conn = sql.connect(config.get('Default','db_path'))

cursor = conn.cursor()

cursor.execute("""
    CREATE TABLE if not exists proxypool(
        proxy TEXT UNIQUE,
        errors INTEGER
    )
""")

conn.commit()

conn.close()