import configparser
import sqlite3 as sql
import logging
import sys
import os
import asyncio


log_levels_list = {
    'CRITICAL': logging.CRITICAL,
    'ERROR': logging.ERROR,
    'WARNING': logging.WARNING,
    'INFO': logging.info,
    'DEBUG': logging.info,
    'NOTSET': logging.NOTSET
}


class ProxyBrokerAdministrator:

    def __init__(self, level_of_log):
        self.__log_level = level_of_log
        self._config = configparser.ConfigParser()
        self._config.read(os.getenv('CONFIG_PATH'))
        self._conn = sql.connect(self._config.get('Default','db_path'))
        self._limit_find_proxies = int(self._config.get('ProxybrokerAdministrator','limit_find_proxies'))
        self._cursor = self._conn.cursor()
        self._subprocess_broker = None


    async def _subprocess_find_proxies(self, count_proxies: int):
        logging.info('Create subprocess, find new proxies')
        return await asyncio.create_subprocess_exec(sys.executable, 'proxy_broker.py', str(count_proxies), self.__log_level, stdout=asyncio.subprocess.PIPE)


    async def _proxychecker(self):
        counter_config = 0
        counter_bad_proxies = 0
        while True:
            logging.info('Start new iteration of proxy checker cycle')

            if self._check_good_proxies() is False:
                self._subprocess_broker = await self._subprocess_find_proxies(self._limit_find_proxies)
                await self._subprocess_broker.communicate()
                self._limit_find_proxies += int(self._config.get('ProxybrokerAdministrator', 'additional_find_proxies'))
                logging.info('New limit find proxies {}'.format(self._limit_find_proxies))
            else:
                logging.info('Old limit find proxies {}'.format(self._limit_find_proxies))

            if counter_bad_proxies >= float(self._config.get('ProxybrokerAdministrator', 'counter_delete_bad_proxies')):
                self._del_bad_proxies()
                self._limit_find_proxies = int(self._config.get('ProxybrokerAdministrator','limit_find_proxies'))
                logging.info('Reset counter bad proxies. New limit find proxies {}'.format(self._limit_find_proxies))
                counter_bad_proxies = 0
            else:
                counter_bad_proxies += 1
                logging.info('Counter bad proxies increment. Current counter bad proxies {}'.format(counter_bad_proxies))

            if counter_config >= float(self._config.get('Default', 'counter_update_config')):
                self._config.read('storage/config.ini')
                counter_config = 0
                logging.info('Reset config counter. Update current config from file')
            else:
                counter_config += 1
                logging.info('Counter config increment. Current counter config {}'.format(counter_config))

            periodic_delay = self._config.get('ProxybrokerAdministrator', 'periodic_delay')
            logging.info('Wait next {} seconds'.format(periodic_delay))
            await asyncio.sleep(float(periodic_delay))


    def _check_good_proxies(self):
        logging.info('Check good proxies in proxypool')
        self._cursor.execute('SELECT COUNT(*) FROM proxypool WHERE errors < ?', self._config.get('ProxybrokerAdministrator', 'max_errors_per_proxy'))
        good_proxies = self._cursor.fetchone()
        if good_proxies[0] <= float(self._limit_find_proxies):
            logging.info('Proxies is not required amount')
            return False
        else:
            logging.info('Proxies is required amount')
            return True


    def _del_bad_proxies(self):
        self._cursor.execute('DELETE FROM proxypool WHERE errors >= ?', self._config.get('ProxybrokerAdministrator', 'max_errors_per_proxy'))
        self._conn.commit()
        logging.info('Delete bad proxies from proxypool')


if __name__ == '__main__':
    if len(sys.argv) > 1:
        if str(sys.argv[1]) not in log_levels_list:
            log_level = sys.argv[1]
        else:
            log_level = sys.argv[1]
    else:
        log_level = log_levels_list['WARNING']

    logging.basicConfig(level=log_level, format='%(filename)s  --- %(levelname)5s: %(message)s', stream=sys.stderr)

    try:
        logging.info('Start proxybroker administrator')
        admin = ProxyBrokerAdministrator(log_level)
        asyncio.run(admin._proxychecker())
    except KeyboardInterrupt:
        logging.info('Program stoped with KeyboardInterrupt')
        if admin._subprocess_broker is not None:
            if admin._subprocess_broker.returncode is None:
                logging.info('Try terminate proxybroker process')
                admin._subprocess_broker.terminate()
                asyncio.sleep(1.0)
                logging.info('Proxybroker process terminated')
    finally:
        logging.info('Program stoped, close db connection')
        admin._conn.close()