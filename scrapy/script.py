# # Импортируем библиотеку, соответствующую типу нашей базы данных 
# import sqlite3
# from pprint import pprint

# # Создаем соединение с нашей базой данных
# # В нашем примере у нас это просто файл базы
# conn = sqlite3.connect('storage/database.db')

# # Создаем курсор - это специальный объект который делает запросы и получает их результаты
# cursor = conn.cursor()


# cursor.execute('SELECT errors FROM proxypool WHERE proxy = :current_proxy', {'current_proxy': 'https://80.23.125.226:3128'})
# cur_errors = cursor.fetchone()
# cur_errors = int(cur_errors[0])
# pprint(cur_errors)

# # Не забываем закрыть соединение с базой данных
# conn.close()

pages = 150
for i in range(1, pages+1):
    print(i)