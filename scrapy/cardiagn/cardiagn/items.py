# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field
from scrapy.loader.processors import TakeFirst, MapCompose, Identity


class CardiagnAllManualsItem(Item):
    page_url = Field(
        output_processor = Identity()
    )
    name_manual = Field(
        output_processor = Identity()
    )
    url_manual = Field(
        output_processor = Identity()
    )
