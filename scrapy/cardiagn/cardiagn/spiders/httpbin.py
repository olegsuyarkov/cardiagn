import scrapy
import configparser
import os
import sqlite3 as sql
import json


class HttpbinSpider(scrapy.Spider):
    name = 'httpbin'
    allowed_domains = ['httpbin.org']
    start_urls = ['https://httpbin.org/anything/{}'.format(i) for i in range(1, 50)]

    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 16,
        'CONCURRENT_REQUESTS_PER_IP': 4,
        'CONCURRENT_REQUESTS': 32,
        'RETRY_HTTP_CODES': [500, 503, 504, 400, 403, 404, 408],
        'RETRY_TIMES': 12,
        'ROBOTSTXT_OBEY': False,
        'COOKIES_ENABLED': True,
        'DOWNLOAD_TIMEOUT': 5,
        'DOWNLOADER_MIDDLEWARES': {
            'cardiagn.middlewares.RandomProxy': 555
        }
    }

    def __init__(self):
        self._config = configparser.ConfigParser()
        self._config.read('../{}'.format(os.getenv('CONFIG_PATH')))
        self._conn = sql.connect('../{}'.format(self._config.get('Default','db_path')))
        self._cursor = self._conn.cursor()

    def parse(self, response):
        resp = json.loads(response.body)

        yield {
            'ip': resp['origin'],
            'url': resp['url']
        }

    def closed(self, reason):
        self._conn.close()
        self.logger.info('Spider Httpbin closed!')
