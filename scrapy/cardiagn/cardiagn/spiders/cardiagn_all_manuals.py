import scrapy
import configparser
import os
import sqlite3 as sql
from ..items import CardiagnAllManualsItem


class CardiagnAllManualsSpider(scrapy.Spider):
    name = 'cardiagn_all_manuals'
    allowed_domains = ['cardiagn.com']

    custom_settings = {
        'CONCURRENT_REQUESTS_PER_DOMAIN': 16,
        'CONCURRENT_REQUESTS_PER_IP': 8,
        'CONCURRENT_REQUESTS': 32,
        'RETRY_HTTP_CODES': [500, 503, 504, 400, 403, 404, 408],
        'RETRY_TIMES': 20,
        'ROBOTSTXT_OBEY': False,
        'COOKIES_ENABLED': False,
        'DOWNLOAD_TIMEOUT': 5,
        'FEED_EXPORT_ENCODING': 'utf-8',
        'DOWNLOADER_MIDDLEWARES': {
            'cardiagn.middlewares.RandomProxy': 555
        }
    }


    def __init__(self):
        self._config = configparser.ConfigParser()
        self._config.read('../{}'.format(os.getenv('CONFIG_PATH')))
        self._conn = sql.connect('../{}'.format(self._config.get('Default','db_path')))
        self._cursor = self._conn.cursor()


    # stage_1
    # def start_requests(self):
    #     pages = 150
    #     url = 'https://cardiagn.com/all-car-service-and-repair-manuals/?_page={}'
    #     for i in range(1, pages+1):
    #         yield scrapy.Request(url = url.format(str(i)), callback = self.parse)
    #     # yield scrapy.Request(url = url.format(str(2)), callback = self.parse)

    def start_requests(self):
        path = '../storage/stage_3.txt'
        with open(path, 'r') as f:
            urls = f.readlines()
        new_urls = []
        for url in urls:
            new_urls.append(url.rstrip('\n'))

        for i in new_urls:
            yield scrapy.Request(url = i, callback = self.parse)

    def parse(self, response):
        list_of_manuals = response.xpath('//div[@class="panel-heading"]')

        for manual in list_of_manuals:
            loader = scrapy.loader.ItemLoader(item=CardiagnAllManualsItem(), selector=manual)

            loader.add_value('page_url', response.request.url)
            loader.add_xpath('name_manual', './/a/text()')
            loader.add_xpath('url_manual', './/a/@href')

            yield loader.load_item()

    def closed(self, reason):
        self._conn.close()
        self.logger.info('Spider Cardiagn closed!')