import csv
import re
import pandas as pd
from pprint import pprint


def stage_1(path):
    pages = 150
    start_urls = set(['https://cardiagn.com/all-car-service-and-repair-manuals/?_page={}'.format(i) for i in range(1, pages+1)])

    list_url = []

    with open(path) as f_obj:
        reader = csv.DictReader(f_obj, delimiter=',')
        for line in reader:
            list_url.append(line['page_url'])

    list_url = set(list_url)
    diff = start_urls.difference(list_url)
    pprint(diff)
    pprint(len(diff))

    with open('storage/stage_1.txt', 'w') as f:
        for item in diff:
            f.write("%s\n" % item)


def stage_1_filter(path):
    with open(path, 'r') as f:
        urls = f.readlines()
    new_urls = []
    for url in urls:
        new_urls.append(url.rstrip('\n'))
    # pprint(new_urls)
    # pprint(len(new_urls))
    return new_urls


def stage_2(path):
    old_urls = stage_1_filter('storage/stage_2.txt')
    old_urls = set(old_urls)

    list_url = []

    with open(path) as f_obj:
        reader = csv.DictReader(f_obj, delimiter=',')
        for line in reader:
            list_url.append(line['page_url'])

    list_url = set(list_url)
    diff = old_urls.difference(list_url)
    pprint(diff)
    pprint(len(diff))

    with open('storage/stage_3.txt', 'w') as f:
        for item in diff:
            f.write("%s\n" % item)


def concat_csv():
    fout=open("storage/all_manuals.csv","a")
    # first file:
    for line in open("storage/all_manuals_stage_1.csv"):
        fout.write(line)
    # now the rest:    
    for num in range(2,5):
        f = open("storage/all_manuals_stage_"+str(num)+".csv")
        f.readline() # skip the header
        for line in f:
            fout.write(line)
        f.close() # not really needed
    fout.close()


def filter_diagrams():
    df = pd.read_csv('storage/all_manuals.csv')
    df1 = df[df['name_manual'].str.contains('Wiring') | df['name_manual'].str.contains('Circuit')]
    df1.to_csv('storage/only_wirings.csv', index=False)


def without_diagrams():
    df = pd.read_csv('storage/without_wirings.csv')
    df2 = df[~df['name_manual'].str.contains('Circuit', na=False)]
    df2.to_csv('storage/without_wirings_2.csv', index=False)

def add_new_columnn():
    to_remove = [
        '+',
        '&',
        'including',
        'And',
        'Body',
        'Shop',
        'PDF',
        'Component',
        'Locations',
        'DiagramS',
        'SYSTEM',
        'CIRCUITS',
        'OEM',
        'Schematics',
        'Supplement',
        'System',
        # 'Diagram',
        'Diagrams',
        'Procedures',
        'Circuit',
        'Overall',
        'Supplement',
        'Dealer',
        'Patriot',
        'Info',
        'Super',
        'Duty',
        'Full',
        'Electronic',
        'Offline',
        'Factory',
        'Workshop',
        'Repair',
        'Owner’s',
        'Manuals',
        # 'Manual',
        'Wiring',
        'Service'
        'Electrical',
        'Troubleshooting',
    ]

    df = pd.read_csv('storage/only_wirings_modify.csv')
    df['folder_name'] = df['name_manual']
    df['folder_name'] = df['folder_name'].str.replace('|'.join(map(re.escape, to_remove)), '')
    # print(df.head())
    df.to_csv('storage/only_wirings_folder_name.csv', index=False)


def add_new_columnn_stage_2():
    to_remove = [
        'Manual',
        'Diagram',
        'Service',
        'Electrical',
        'Owners'
    ]

    df = pd.read_csv('storage/only_wirings_folder_name.csv')
    df['folder_name'] = df['folder_name'].str.replace('|'.join(map(re.escape, to_remove)), '')
    # print(df.head())
    df.to_csv('storage/only_wirings_folder_name_stage_2.csv', index=False)


if __name__ == '__main__':
    # stages
    # filename = 'all_manuals_stage_3.csv'
    # path = 'storage/{}'.format(filename)
    # stage_2(path)

    # concat all csv
    # concat_csv()

    # filter diagrams
    # filter_diagrams()
    # without_diagrams()
    
    add_new_columnn_stage_2()